/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosophize.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/29 17:47:48 by aalkhiro          #+#    #+#             */
/*   Updated: 2023/01/25 15:18:57 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

int	is_end(t_philo *philo)
{
	int	end;

	end = 0;
	pthread_mutex_lock(&philo->data->data_safe);
	end = philo->data->end;
	pthread_mutex_unlock(&philo->data->data_safe);
	return (end);
}

void	pick_forks(t_philo *philo)
{
	pthread_mutex_lock(&philo->data->forks_mutex[philo->fork1]);
	pthread_mutex_lock(&philo->data->forks_mutex[philo->fork2]);
	scribe(philo, " has taken a fork\n");
	scribe(philo, " has taken a fork\n");
	scribe(philo, " is eating\n");
	pthread_mutex_lock(philo->mutex);
	philo->t_last_meal = chronos(philo->t_start);
	pthread_mutex_unlock(philo->mutex);
	philo->t_last_act = chronos(philo->t_start);
	philo->meals2eat--;
	if (philo->meals2eat > -1 && philo->meals2eat == 0)
	{
		pthread_mutex_lock(&philo->data->data_safe);
		if (++philo->data->plates >= philo->data->nbr_philo)
			philo->data->end = 1;
		pthread_mutex_unlock(&philo->data->data_safe);
	}
	ft_sleep(philo, philo->t_eat);
	pthread_mutex_unlock(&philo->data->forks_mutex[philo->fork2]);
	pthread_mutex_unlock(&philo->data->forks_mutex[philo->fork1]);
}

void	bed(t_philo *philo)
{
	scribe(philo, " is sleeping\n");
	philo->t_last_act = chronos(philo->t_start);
	ft_sleep(philo, philo->t_sleep);
}

void	think(t_philo *philo)
{
	scribe(philo, " is thinking\n");
	philo->t_last_act = chronos(philo->t_start);
	ft_sleep(philo, philo->t_think);
}

void	*philosophizing(void *phil)
{
	t_philo	*philo;
	int		end;

	end = 0;
	philo = (t_philo *)phil;
	pthread_mutex_lock(&philo->data->data_safe);
	pthread_mutex_unlock(&philo->data->data_safe);
	if (philo->fork1 == philo->fork2)
	{
		ft_sleep(philo, philo->t_die + 1);
		end = is_end(philo);
	}
	if (philo->id % 2)
		usleep(philo->t_eat / 2);
	while (!end)
	{
		pick_forks(philo);
		bed(philo);
		think(philo);
		end = is_end(philo);
	}
	return (0);
}
