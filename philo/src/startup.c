/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   startup.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/29 17:46:48 by aalkhiro          #+#    #+#             */
/*   Updated: 2023/01/25 15:37:42 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

void	apocalips(t_data *data, int i)
{
	pthread_mutex_lock(&data->data_safe);
	data->end = 1;
	pthread_mutex_unlock(&data->data_safe);
	while (i >= 0)
		pthread_join(data->philos[--i].thread, NULL);
}

int	check_pulse(t_data *data)
{
	int	i;
	int	end;

	end = 0;
	i = -1;
	while (!end && ++i < data->nbr_philo)
	{
		pthread_mutex_lock(&data->philo_mutex[i]);
		if ((data->philos[i].t_last_meal + data->t_die)
			<= chronos(data->t_start))
		{
			end = 1;
			pthread_mutex_lock(&data->data_safe);
			data->end = end;
			printf("%ld %d%s", chronos(data->t_start),
				data->philos[i].id + 1, " died\n");
			pthread_mutex_unlock(&data->data_safe);
		}
		pthread_mutex_unlock(&data->philo_mutex[i]);
		pthread_mutex_lock(&data->data_safe);
		end = data->end;
		pthread_mutex_unlock(&data->data_safe);
	}
	return (end);
}

void	check_end(t_data *data)
{
	int	end;

	end = 0;
	while (!end)
	{
		if (check_pulse(data))
			break ;
		usleep(100);
	}
}

void	startup(t_data *data)
{
	int		i;

	i = -1;
	pthread_mutex_lock(&data->data_safe);
	while (++i < data->nbr_philo)
	{
		if (pthread_create(&data->philos[i].thread, NULL,
				philosophizing, (void *)(&data->philos[i])))
		{
			pthread_mutex_unlock(&data->data_safe);
			apocalips(data, i);
			error("Thread creation error\n", data);
		}
	}
	pthread_mutex_unlock(&data->data_safe);
	check_end(data);
	while (i > 0)
		pthread_join(data->philos[--i].thread, NULL);
}
