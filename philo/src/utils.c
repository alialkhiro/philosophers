/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/29 18:14:51 by aalkhiro          #+#    #+#             */
/*   Updated: 2023/01/25 14:07:28 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

long int	chronos(long int start)
{
	struct timeval	t;

	if (gettimeofday(&t, NULL))
		return (0);
	return (t.tv_usec / 1000 + t.tv_sec * 1000 - start);
}

int	scribe(t_philo *philo, char *str)
{
	int	end;

	pthread_mutex_lock(&philo->data->data_safe);
	end = philo->data->end;
	if (!end)
		printf("%ld %d%s", chronos(philo->t_start), philo->id + 1, str);
	pthread_mutex_unlock(&philo->data->data_safe);
	return (end);
}

int	ft_isnumber(char c)
{
	if (c >= '0' && c <= '9')
		return (1);
	return (0);
}

void	ft_sleep(t_philo *philo, int t_limit)
{
	int	end;

	end = 0;
	while ((chronos(philo->t_start) < t_limit + philo->t_last_act - 1))
	{
		pthread_mutex_lock(&philo->data->data_safe);
		end = philo->data->end;
		pthread_mutex_unlock(&philo->data->data_safe);
		if (end)
			break ;
		usleep(1000);
	}
	if (!end)
		while ((chronos(philo->t_start) < t_limit + philo->t_last_act))
			usleep(100);
}
