/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/20 09:40:59 by aalkhiro          #+#    #+#             */
/*   Updated: 2023/01/25 15:00:39 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

int	ft_strlen(char *str)
{
	int	i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

void	mem_clean(t_data *data)
{
	if (data)
	{
		if (data->forks_mutex)
			free(data->forks_mutex);
		if (data->philo_mutex)
			free(data->philo_mutex);
		if (data->philos)
			free(data->philos);
		free(data);
	}
}

void	error(char *err_msg, t_data *data)
{
	printf("%s", err_msg);
	mem_clean(data);
	exit (1);
}

void	check_args(int argc, char **argv)
{
	int	i;
	int	j;

	if (argc < 5 || argc > 6)
		error("Incorrect number of arguements\n", NULL);
	i = 0;
	while (++i < argc)
	{
		j = 0;
		while (argv[i][j])
			if (!ft_isnumber(argv[i][j++]) || ft_strlen(argv[i]) > 10)
				error("Invalid arguement\n", NULL);
	}
}

int	main(int argc, char **argv)
{
	t_data	*data;

	check_args(argc, argv);
	data = malloc(sizeof(t_data));
	if (!data)
		return (1);
	init(data, argc, argv);
	startup(data);
	mem_clean(data);
	return (0);
}
