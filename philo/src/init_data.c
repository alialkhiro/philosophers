/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_data.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/29 19:21:55 by aalkhiro          #+#    #+#             */
/*   Updated: 2023/01/25 12:36:49 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

int	ft_atoi(char *str)
{
	int	nbr;
	int	i;

	i = 0;
	nbr = 0;
	while (str[i])
		nbr = nbr * 10 + (str[i++] - '0');
	return (nbr);
}

void	philo_init(t_philo *philo, t_data *data, int id)
{
	philo->id = id;
	philo->t_last_meal = 0;
	philo->t_last_act = 0;
	philo->fork1 = (id + id % 2) % data->nbr_philo;
	philo->fork2 = (id + 1 - id % 2) % data->nbr_philo;
	philo->data = data;
	philo->meals2eat = data->meals2eat;
	philo->t_start = data->t_start;
	philo->mutex = &data->philo_mutex[id];
	philo->t_limit = 0;
	philo->t_eat = data->t_eat;
	philo->t_sleep = data->t_sleep;
	philo->t_die = data->t_die;
	philo->t_think = (philo->t_die - philo->t_eat - philo->t_sleep) / 2;
}

void	init_arr(t_data *data)
{
	int	i;

	if (pthread_mutex_init(&data->data_safe, NULL))
		error("Mutex initialization error\n", data);
	i = -1;
	while (++i < data->nbr_philo)
		if (pthread_mutex_init(&data->forks_mutex[i], NULL)
			|| pthread_mutex_init(&data->philo_mutex[i], NULL))
			error("Mutex initialization error\n", data);
	i = -1;
	while (++i < data->nbr_philo)
		philo_init(&data->philos[i], data, i);
}

void	init_data(t_data *data, int argc, char **argv)
{
	data->forks_mutex = NULL;
	data->philo_mutex = NULL;
	data->philos = NULL;
	data->end = 0;
	data->plates = 0;
	data->nbr_philo = ft_atoi(argv[1]);
	if (data->nbr_philo == 0 || data->nbr_philo > 200)
		error("Invalid number of philosophers\n", data);
	data->t_die = ft_atoi(argv[2]);
	data->t_eat = ft_atoi(argv[3]);
	data->t_sleep = ft_atoi(argv[4]);
	if (data->t_die < 60 || data->t_eat < 60 || data->t_sleep < 60)
		error("Invalid time value\n", data);
	if (argc == 6)
	{
		data->meals2eat = ft_atoi(argv[5]);
		if (data->meals2eat < 0)
			error("Invalid meal value\n", data);
	}
	else
		data->meals2eat = -1;
}

void	init(t_data *data, int argc, char **argv)
{
	init_data(data, argc, argv);
	data->philos = malloc(sizeof(t_philo) * data->nbr_philo);
	data->forks_mutex = (pthread_mutex_t *)malloc
		(sizeof(pthread_mutex_t) * (data->nbr_philo));
	data->philo_mutex = (pthread_mutex_t *)malloc
		(sizeof(pthread_mutex_t) * (data->nbr_philo));
	if (!data->philos || !data->forks_mutex || !data->philo_mutex)
		error("Memory error\n", data);
	data->t_start = chronos(0);
	init_arr(data);
}
