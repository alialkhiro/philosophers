/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosophers.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/20 09:41:13 by aalkhiro          #+#    #+#             */
/*   Updated: 2023/01/25 12:36:54 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILOSOPHERS_H
# define PHILOSOPHERS_H

# include <stdio.h>
# include <unistd.h>
# include <stdlib.h>
# include <pthread.h>
# include <sys/time.h>
# include <limits.h>

typedef struct s_philosopher
{
	int				id;
	int				t_last_meal;
	long int		t_start;
	int				t_die;
	int				t_eat;
	int				t_sleep;
	int				t_think;
	int				meals2eat;
	int				t_last_act;
	int				t_limit;
	int				fork1;
	int				fork2;
	pthread_t		thread;
	struct s_data	*data;
	pthread_mutex_t	*mutex;
}	t_philo;

typedef struct s_data
{
	int						nbr_philo;
	int						t_die;
	int						t_eat;
	int						t_sleep;
	int						meals2eat;
	int						end;
	int						plates;
	long int				t_start;
	t_philo					*philos;
	pthread_mutex_t			*forks_mutex;
	pthread_mutex_t			*philo_mutex;
	pthread_mutex_t			data_safe;
}	t_data;

void		init(t_data *data, int argc, char **argv);
void		startup(t_data *data);
void		*philosophizing(void *phil);
void		error(char *err_msg, t_data *data);

int			is_end(t_philo *philo);
long int	chronos(long int start);
int			ft_isnumber(char c);
int			scribe(t_philo *philo, char *str);
void		ft_sleep(t_philo *philo, int t_limit);

#endif
